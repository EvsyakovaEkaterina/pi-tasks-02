#define _CRT_SECURE_NO_WARNINGS
#define N 40
#define K 2
#include <stdio.h>
#include <string.h>
#include <time.h>
int main()
{
	int i, j, title,uppercase,numeral,takt; 
	int length;
	char s[256];
	printf("Enter size of parols: ");
	scanf("%i", &length);
	srand(time(0));
	for (i = 0; i < N; ++i) 
	{
		title = 0; uppercase = 0; numeral = 0;
		while (title == 0 || uppercase == 0 || numeral == 0) 
		{
			title = 0;  uppercase = 0; numeral = 0;
			for (j=0; j<length; ++j) 
			{
				takt = rand() % 3;
				if (takt == 0) 
				{
					s[j] = rand() %('Z'-'A'+1)+'A';
					title = 1;
				}
				if (takt == 1) 
				{
					s[j] = rand() % ('z'-'a'+1)+'a';
					uppercase = 1;
				}
				if (takt == 2) 
				{
					s[j] = rand() %('9'-'0'+1)+'0';
					numeral= 1;
				}
			}
		}
		for (j = 0; j < length; ++j)
			printf("%c", s[j]);
		if ((i + 1) % K == 0)
			printf("\n");
		else
			printf("   ");
	}
	return 0;
}